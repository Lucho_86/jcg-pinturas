//console.log("funcionando");
$("#contactForm").submit(function(event){
    event.preventDefault(); //almacena los datos sin refrescar el sitio web.
    enviar();
});
function enviar(){
    //console.log("ejecutado");
    var datos = $("#contactForm").serialize(); //toma los datos "name" y los lleva a un arreglo.
    $.ajax({
        type: "post",
        url:"extras-form/formulario.php",
        data: datos,
        success: function(texto){
            if(texto=="exito"){                
                correcto();
                // redirec();
            }else{ 
                phperror(texto);
            }
        }
    })
}
function correcto(){
    const textoOk = 'Mensaje enviado correctamente';
    $("#mensajeExito").removeClass("d-none");
    $("#mensajeExito").html(textoOk);
    $("#mensajeError").addClass("d-none");
    $('#contactForm').trigger("reset");
}
function phperror(texto){
    $("#mensajeError").removeClass("d-none");
    $("#mensajeError").html(texto);
}

function redirec(){
    window.location = "https://www.google.com";
}
